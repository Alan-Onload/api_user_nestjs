import { Body, Controller, Get, Post } from '@nestjs/common';
import { User } from '@prisma/client';
import { UserDTO } from './models/user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly service: UserService) {}

  @Get()
  async findAll(): Promise<User[]> {
    return await this.service.findAll();
  }
  @Post()
  async postUser(@Body() user: UserDTO): Promise<User> {
    return this.service.postUser(user);
  }
}
