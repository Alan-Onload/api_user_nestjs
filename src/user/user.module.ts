import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { Validacao } from './middleware/validacao';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  controllers: [UserController],
  providers: [UserService, PrismaService],
})
export class UserModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(Validacao)
      .forRoutes({ path: 'user', method: RequestMethod.POST });
  }
}
