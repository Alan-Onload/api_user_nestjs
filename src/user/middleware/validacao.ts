import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class Validacao implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    if (
      req.body.email == null ||
      req.body.email == '' ||
      req.body.email == undefined
    ) {
      res.json({ err: 'o campo email é obrigatório' }).status(500);
    }
    next();
  }
}
